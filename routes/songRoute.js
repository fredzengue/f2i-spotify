const { getAllSongs, createSong, getSong, UpdateSong, deleteSong } = require("../controllers/songController");
  
  const router = require("express").Router();
  
  router.get("/all", getAllSongs);
  
  router.post("/new", createSong);
  router.route('/:id')
  .get(getSong)
  .put(UpdateSong)
  .delete(deleteSong)
  module.exports = router;