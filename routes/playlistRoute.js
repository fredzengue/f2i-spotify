const { getAllPlaylists, createPlaylist, getPlaylist, UpdatePlaylist, deletePlaylist } = require("../controllers/playlistController");
  
  const router = require("express").Router();
  
  router.get("/all", getAllPlaylists);
  
  router.post("/new", createPlaylist);
  router.route('/:id')
  .get(getPlaylist)
  .put(UpdatePlaylist)
  .delete(deletePlaylist)
  module.exports = router;