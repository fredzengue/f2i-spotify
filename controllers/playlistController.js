const Playlist = require("../models/playlist");
const Song = require("../models/song");
const getAllPlaylists = async (req, res) => {
  try {
    const playlists = await Playlist.find()
      .populate('songs user');
    res.status(201).json(playlists);
  } catch (error) {
    res.status(500).json({ msg: "Nous avons rencontrer une erreur" });
  }
};

const createPlaylist = async (req, res) => {
  try {
    const newPlaylist = await new Playlist(req.body);
    newPlaylist.save();
    res.status(201).json(newPlaylist);
  } catch (error) {
    res.status(500).json({ msg: "Nous avons rencontrer une erreur" });
  }
};
const getPlaylist = async (req, res) => {
  const id = req.params.id;
  try {
    const playlist =  await Playlist.findById(id).populate('songs user');
    res.status(201).json(playlist);
  } catch (error) {
    res.status(500).json({ msg: "Nous avons rencontrer une erreur" });
  }
};

const UpdatePlaylist = async (req, res, next) => {
  try {
    const playlistId = req.params.id;
    const { name, songs } = req.body;

    const playlist = await Playlist.findById(playlistId).populate('songs user');

    if (!playlist) {
      return res.status(404).json({ error: 'Playlist non trouvée' });
    }

    if (songs) {
      const newSongs = await Song.find({ _id: { $in: songs } });
      if (newSongs.length !== songs.length) {
        return res.status(404).json({ error: 'Certaines chansons sont introuvables' });
      }
      playlist.songs = newSongs;
    }

    if (name) {
      playlist.name = name;
    }

    await playlist.save();

    res.json(playlist);
  } catch (error) {
    res.status(500).json({ error: 'Une erreur est survenue lors de la mise à jour de la playlist' });
  }
};
const deletePlaylist = async (req, res, next) => {
  try {
    const playlistId = req.params.id;
    const playlist = await Playlist.findByIdAndDelete(playlistId);

    if (!playlist) {
      return res.status(404).json({ error: 'Playlist non trouvée' });
    }

    res.json({ message: 'Playlist supprimée avec succès' });
  } catch (error) {
    res.status(500).json({ error: 'Une erreur est survenue lors de la suppression de la playlist' });
  }

};
module.exports = { getAllPlaylists, createPlaylist, getPlaylist, UpdatePlaylist, deletePlaylist };