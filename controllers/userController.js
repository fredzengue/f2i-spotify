const User = require("../models/user");
const jwt = require("jwt-simple");
const config = require("../config")


const register = async (req, res) => {
  try {
    console.log(req.body);
    const { firstName, lastName, email, password } = req.body;
    
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(400).json({ message: 'Cet utilisateur existe déjà.' });
    }
    const newUser = new User({ firstName, lastName, email, password});
    await newUser.hashPassword();
   
    await newUser.save();
    res.status(201).json({ message: 'Utilisateur créé avec succès.', user: newUser });
  } catch (error) {
    res.status(500).json({ msg: "Erreur lors de l'inscription" });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;
  console.log(req.body);
  try {
    const user = await User.findOne({ email })
    if (!user) {
      return res.status(401).json({ message: "Nom d'utilisateur incorrect." })
    }
    const passwordMatch = await user.comparePassword(password)
    if (!passwordMatch) {
      return res.status(401).json({ message: "Mot de passe incorrecte." })
    }
    const token = jwt.encode({ id: user.id }, config.jwtSecret)
    res.status(201).json({ user, token })
  } catch (error) {
    res.status(500).json({ msg: "Erreur lors de la connexion" })
  }
};
module.exports = { register, login };