const Song = require("../models/song");
const getAllSongs = async (req, res) => {
  try {
    const songs = await Song.find().populate('artist');
    res.json(songs);
  } catch (error) {
    res.status(500).json({ error: 'Une erreur est survenue lors de la récupération des chansons.' });
  }
};

const createSong = async (req, res) => {
  try {
    const newSong = await new Song(req.body);
    newSong.artist = req.user.id;
    newSong.save();
    res.json(newSong);
  } catch (error) {
    res.status(500).json({ msg: "Nous avons rencontrer une erreur" });
  }
};
const getSong = async (req, res) => {
  const id = req.params.id;
  try {
    const song = await Song.findById(id).populate('artist');
    if (!song) {
      return res.status(404).json({ error: 'Chanson non trouvée.' });
    }
    res.json(song);
  } catch (error) {
    res.status(500).json({ error: 'Une erreur est survenue lors de la récupération de la chanson.' });
  }
};

const UpdateSong = async (req, res, next) => {
  const id = req.params.id;

  try {
    const { url, rating } = req.body;
    const song = await Song.findByIdAndUpdate(id, { url, rating }, { new: true });
    if (!song) {
      return res.status(404).json({ error: 'Chanson non trouvée.' });
    }
    res.json(song);
  } catch (error) {
    res.status(500).json({ error: 'Une erreur est survenue lors de la mise à jour de la chanson.' });
  }
};
const deleteSong = async (req, res, next) => {
  const id = req.params.id;
  try {
    const song = await Song.findByIdAndRemove(id);
    if (!song) {
      return res.status(404).json({ error: 'Chanson non trouvée.' });
    }
    res.json({ message: 'Chanson supprimée avec succès.' });
  } catch (error) {
    res.status(500).json({ error: 'Une erreur est survenue lors de la suppression de la chanson.' });
  }
};
module.exports = { getAllSongs, createSong, getSong, UpdateSong, deleteSong };