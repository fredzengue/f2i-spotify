const express = require("express");
const app = express();

const port = process.env.PORT || 5000;

const mongoose = require("mongoose");
const userRouter = require("./routes/userRoute");
const playlistRouter = require("./routes/playlistRoute");
const songRouter = require("./routes/songRoute");
const passportJWT = require('./middlewares/passportJWT')()

main().catch((err) => console.log(err));

async function main() {
  await mongoose.connect("mongodb://127.0.0.1:27017/f2i-spotify");
  console.log(`📚[Database] is connected to MongoDB`);
}
// app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(passportJWT.initialize())
app.post("/test", (req, res) => {
  console.log(req.body)
    res.send("🐲");
});
app.use("/api/users", userRouter);
app.use("/api/playlist",passportJWT.authenticate(), playlistRouter);
app.use("/api/song",passportJWT.authenticate(), songRouter);

app.listen(port, () => console.log(` ⚡️ [SERVER] is running on : ${port}`));