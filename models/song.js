const { mongoose, Schema } = require("mongoose");

const songSchema = new Schema({
    url: String,
    rating: Number,
    artist: { type: Schema.Types.ObjectId, ref: "User" },
});

const Song = mongoose.model("Song", songSchema);

module.exports = Song;